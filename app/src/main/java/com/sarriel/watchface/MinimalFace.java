package com.sarriel.watchface;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.content.ContextCompat;
import android.support.wearable.watchface.CanvasWatchFaceService;
import android.support.wearable.watchface.WatchFaceStyle;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.WindowInsets;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Digital watch face with seconds. In ambient mode, the seconds aren't displayed. On devices with
 * low-bit ambient mode, the text is drawn without anti-aliasing in ambient mode.
 * <p>
 * Important Note: Because watch face apps do not have a default Activity in
 * their project, you will need to set your Configurations to
 * "Do not launch Activity" for both the Wear and/or Application modules. If you
 * are unsure how to do this, please review the "Run Starter project" section
 * in the Google Watch Face Code Lab:
 * https://codelabs.developers.google.com/codelabs/watchface/index.html#0
 */
public class MinimalFace extends CanvasWatchFaceService {

    private static final Typeface NORMAL_TYPEFACE = Typeface.create(Typeface.SANS_SERIF, Typeface.NORMAL);

    private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm", Locale.getDefault());
    private SimpleDateFormat dayFormat = new SimpleDateFormat("E d ", Locale.getDefault());
    private SimpleDateFormat monthFormat = new SimpleDateFormat("MMM", Locale.getDefault());
    private SimpleDateFormat amPmFormat = new SimpleDateFormat("a", Locale.getDefault());
    private boolean is24hourFormat;

    /**
     * Update rate in milliseconds for interactive mode. Defaults to one second
     * because the watch face needs to update seconds in interactive mode.
     */
    private static final long INTERACTIVE_UPDATE_RATE_MS = TimeUnit.MILLISECONDS.toMillis(42);

    /**
     * Handler message id for updating the time periodically in interactive mode.
     */
    private static final int MSG_UPDATE_TIME = 0;

    @Override
    public Engine onCreateEngine() {
        return new Engine();
    }

    private static class EngineHandler extends Handler {
        private final WeakReference<MinimalFace.Engine> mWeakReference;

        public EngineHandler(MinimalFace.Engine reference) {
            mWeakReference = new WeakReference<>(reference);
        }

        @Override
        public void handleMessage(Message msg) {
            MinimalFace.Engine engine = mWeakReference.get();
            if (engine != null) {
                switch (msg.what) {
                    case MSG_UPDATE_TIME:
                        engine.handleUpdateTimeMessage();
                        break;
                }
            }
        }
    }

    private class Engine extends CanvasWatchFaceService.Engine {

        private final Handler mUpdateTimeHandler = new EngineHandler(this);
        private Calendar mCalendar;
        private Date date = new Date();
        private final BroadcastReceiver mTimeZoneReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                mCalendar.setTimeZone(TimeZone.getDefault());
                invalidate();
            }
        };
        private boolean mRegisteredTimeZoneReceiver = false;
        private Rect timeBounds = new Rect();
        private Rect dateBounds = new Rect();
        private Paint mBackgroundPaint;
        private Paint mTimePaint;
        private Paint mTimePaintAmbient;
        private Paint mDayPaint;
        private Paint mDayPaintAmbient;
        private Paint mMonthPaint;
        private Paint mLinePaint;
        private float dotSize;
        /**
         * Whether the display supports fewer bits for each color in ambient mode. When true, we
         * disable anti-aliasing in ambient mode.
         */
        private boolean mLowBitAmbient;
        private boolean mBurnInProtection;
        private boolean mAmbient;

        @Override
        public void onCreate(SurfaceHolder holder) {
            super.onCreate(holder);

            setWatchFaceStyle(new WatchFaceStyle.Builder(MinimalFace.this)
                    .setAcceptsTapEvents(true)
                    .build());

            mCalendar = Calendar.getInstance();

            // Initializes background.
            mBackgroundPaint = new Paint();
            mBackgroundPaint.setColor(ContextCompat.getColor(getApplicationContext(), R.color.watchface_background));

            mLinePaint = new Paint();
            mLinePaint.setAntiAlias(true);
            mLinePaint.setColor(getColor(R.color.watchface_dot));
            mLinePaint.setStrokeWidth(8);
            mLinePaint.setStyle(Paint.Style.STROKE);
            mLinePaint.setStrokeCap(Paint.Cap.ROUND);

            // Initializes Time Paint
            mTimePaint = new Paint();
            mTimePaint.setAntiAlias(true);
            mTimePaint.setColor(getColor(R.color.watchface_time));
            mTimePaint.setTypeface(NORMAL_TYPEFACE);
            mTimePaint.setTextAlign(Paint.Align.CENTER);

            // Initializes Time Paint in ambient mode
            mTimePaintAmbient = new Paint();
            mTimePaintAmbient.setAntiAlias(true);
            mTimePaintAmbient.setColor(getColor(R.color.watchface_time_ambient));
            mTimePaintAmbient.setStrokeWidth(1.8f);
            mTimePaintAmbient.setStyle(Paint.Style.STROKE);
            mTimePaintAmbient.setTextAlign(Paint.Align.CENTER);
            mTimePaintAmbient.setTypeface(NORMAL_TYPEFACE);

            // Initialize Date Paint
            mDayPaint = new Paint();
            mDayPaint.setTypeface(NORMAL_TYPEFACE);
            mDayPaint.setAntiAlias(true);
            mDayPaint.setColor(getColor(R.color.watchface_day));
            mDayPaint.setTextAlign(Paint.Align.RIGHT);

            // Initialize Date Paint in ambient
            mDayPaintAmbient = new Paint();
            mDayPaintAmbient.setTypeface(NORMAL_TYPEFACE);
            mDayPaintAmbient.setAntiAlias(true);
            mDayPaintAmbient.setColor(getColor(R.color.watchface_day_ambient));
            mDayPaintAmbient.setTextAlign(Paint.Align.RIGHT);

            // Initialize Date Paint
            mMonthPaint = new Paint();
            mMonthPaint.setTypeface(NORMAL_TYPEFACE);
            mMonthPaint.setAntiAlias(true);
            mMonthPaint.setColor(getColor(R.color.watchface_month));
            mMonthPaint.setTextAlign(Paint.Align.RIGHT);
        }

        @Override
        public void onDestroy() {
            mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME);
            super.onDestroy();
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);

            if (visible) {
                registerReceiver();

                // Update time zone in case it changed while we weren't visible.
                mCalendar.setTimeZone(TimeZone.getDefault());

                // Setup 12/24 hr mode
                is24hourFormat = DateFormat.is24HourFormat(getApplicationContext());

                invalidate();
            } else {
                unregisterReceiver();
            }

            // Whether the timer should be running depends on whether we're visible (as well as
            // whether we're in ambient mode), so we may need to start or stop the timer.
            updateTimer();
        }

        private void registerReceiver() {
            if (mRegisteredTimeZoneReceiver) {
                return;
            }
            mRegisteredTimeZoneReceiver = true;
            IntentFilter filter = new IntentFilter(Intent.ACTION_TIMEZONE_CHANGED);
            MinimalFace.this.registerReceiver(mTimeZoneReceiver, filter);
        }

        private void unregisterReceiver() {
            if (!mRegisteredTimeZoneReceiver) {
                return;
            }
            mRegisteredTimeZoneReceiver = false;
            MinimalFace.this.unregisterReceiver(mTimeZoneReceiver);
        }

        @Override
        public void onApplyWindowInsets(WindowInsets insets) {
            super.onApplyWindowInsets(insets);

            // Load resources that have alternate values for round watches.
            Resources resources = MinimalFace.this.getResources();
            float timeSize = resources.getDimension(R.dimen.digital_time_size);
            float dateSize = resources.getDimension(R.dimen.digital_date_size);
            dotSize = resources.getDimension(R.dimen.digital_dot_size);

            mLinePaint.setStrokeWidth(dotSize);

            mTimePaint.setTextSize(timeSize);
            mTimePaintAmbient.setTextSize(timeSize);

            mDayPaint.setTextSize(dateSize);
            mDayPaintAmbient.setTextSize(dateSize);

            mMonthPaint.setTextSize(dateSize);

        }

        @Override
        public void onPropertiesChanged(Bundle properties) {
            super.onPropertiesChanged(properties);
            mLowBitAmbient = properties.getBoolean(PROPERTY_LOW_BIT_AMBIENT, false);
            mBurnInProtection = properties.getBoolean(PROPERTY_BURN_IN_PROTECTION, false);
        }

        @Override
        public void onTimeTick() {
            super.onTimeTick();
            invalidate();
        }

        @Override
        public void onAmbientModeChanged(boolean inAmbientMode) {
            super.onAmbientModeChanged(inAmbientMode);

            mAmbient = inAmbientMode;
            if (mLowBitAmbient) {
                mTimePaint.setAntiAlias(!inAmbientMode);
                mTimePaintAmbient.setAntiAlias(!inAmbientMode);
            }

            // Whether the timer should be running depends on whether we're visible (as well as
            // whether we're in ambient mode), so we may need to start or stop the timer.
            updateTimer();
        }

        /**
         * Captures tap event (and tap type) and toggles the background color if the user finishes
         * a tap.
         */
        @Override
        public void onTapCommand(int tapType, int x, int y, long eventTime) {
            switch (tapType) {
                case TAP_TYPE_TOUCH:
                    // The user has started touching the screen.
                    break;
                case TAP_TYPE_TOUCH_CANCEL:
                    // The user has started a different gesture or otherwise cancelled the tap.
                    break;
                case TAP_TYPE_TAP:
                    // The user has completed the tap gesture.
                    toggleFullBrightness();
                    break;
            }
            invalidate();
        }

        @Override
        public void onDraw(Canvas canvas, Rect bounds) {

            // update date
            date.setTime(System.currentTimeMillis());

            // Draw the background.
            canvas.drawColor(Color.BLACK);

            // Draw H:MM
            String timeString = timeFormat.format(date);
            mTimePaint.getTextBounds("00:00", 0, timeString.length(), timeBounds);
            canvas.drawText(
                    timeString,
                    bounds.centerX(),
                    bounds.centerY() + timeBounds.height() / 2,
                    mAmbient ? mTimePaintAmbient : mTimePaint
            );

            // Draw month
            String monthString = monthFormat.format(date);
            monthString = monthString.substring(0, 1).toUpperCase() + monthString.substring(1);
            mMonthPaint.getTextBounds(monthString, 0, monthString.length(), dateBounds);
            mMonthPaint.setTextAlign(Paint.Align.RIGHT);
            float monthOffset = bounds.centerX() + timeBounds.width() / 2;
            float monthWidth = dateBounds.width();
            canvas.drawText(
                    monthString,
                    bounds.centerX() + timeBounds.width() / 2,
                    bounds.centerY() + (timeBounds.height() / 2) + mTimePaint.descent() * 1.4f,
                    mMonthPaint
            );

            // Draw AM / PM
            if (!is24hourFormat) {
                String amPmString = amPmFormat.format(date);
                //amPmString = amPmString.substring(0, 1).toUpperCase() + monthString.substring(1);
                mMonthPaint.getTextBounds(monthString, 0, amPmString.length(), dateBounds);
                mMonthPaint.setTextAlign(Paint.Align.LEFT);
                mDayPaintAmbient.setTextAlign(Paint.Align.LEFT);
                float ampmOffset = bounds.centerX() + timeBounds.width() / 2;
                float ampmWidth = dateBounds.width();
                canvas.drawText(
                        amPmString,
                        bounds.centerX() - timeBounds.width() / 2,
                        bounds.centerY() + (timeBounds.height() / 2) + mTimePaint.descent() * 1.4f,
                        mAmbient ? mDayPaintAmbient : mMonthPaint
                );
            }

            // Draw day
            String dayString = dayFormat.format(date);
            dayString = dayString.substring(0, 1).toUpperCase() + dayString.substring(1);
            mDayPaint.getTextBounds(dayString, 0, monthString.length(), dateBounds);
            mDayPaintAmbient.setTextAlign(Paint.Align.RIGHT);
            canvas.drawText(
                    dayString,
                    monthOffset - monthWidth,
                    bounds.centerY() + (timeBounds.height() / 2) + mTimePaint.descent() * 1.4f,
                    mAmbient ? mDayPaintAmbient : mDayPaint
            );

            // Show second dot
            if (!mAmbient) {
                double fill = date.getTime() % 60000.0 / 60000.0;
                double start = Math.PI * -0.5;
                double stop = start + Math.PI * 2 * fill;
                double radius = bounds.centerX() - 10 - dotSize;

                float x = (float) (bounds.centerX() + radius * Math.cos(stop));
                float y = (float) (bounds.centerY() + radius * Math.sin(stop));
                canvas.drawPoint(x, y, mLinePaint);
            }
        }

        /**
         * Starts the {@link #mUpdateTimeHandler} timer if it should be running and isn't currently
         * or stops it if it shouldn't be running but currently is.
         */
        private void updateTimer() {
            mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME);
            if (shouldTimerBeRunning()) {
                mUpdateTimeHandler.sendEmptyMessage(MSG_UPDATE_TIME);
            }
        }

        /**
         * Returns whether the {@link #mUpdateTimeHandler} timer should be running. The timer should
         * only run when we're visible and in interactive mode.
         */
        private boolean shouldTimerBeRunning() {
            return isVisible() && !isInAmbientMode();
        }

        /**
         * Handle updating the time periodically in interactive mode.
         */
        private void handleUpdateTimeMessage() {
            invalidate();
            if (shouldTimerBeRunning()) {
                long timeMs = System.currentTimeMillis();
                long delayMs = INTERACTIVE_UPDATE_RATE_MS - (timeMs % INTERACTIVE_UPDATE_RATE_MS);
                mUpdateTimeHandler.sendEmptyMessageDelayed(MSG_UPDATE_TIME, delayMs);
            }
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        is24hourFormat = DateFormat.is24HourFormat(getApplicationContext());
        timeFormat = is24hourFormat
                ? new SimpleDateFormat("HH:mm", Locale.getDefault())
                : new SimpleDateFormat("hh:mm", Locale.getDefault())
        ;
        dayFormat = new SimpleDateFormat("E d ", Locale.getDefault());
        monthFormat = new SimpleDateFormat("MMM", Locale.getDefault());
    }

    private void toggleFullBrightness() {

//        int SysBackLightValue = (255);
//        android.provider.Settings.System.putInt(
//                getContentResolver(),
//                android.provider.Settings.System.SCREEN_BRIGHTNESS,
//                SysBackLightValue
//        );

    }

}

// CIRCLE
//                Path path = new Path();
//                for (double i = start; i <= stop; i += 0.01) {
//                    float x = (float) (bounds.centerX() + (bounds.centerX() - 20) * Math.cos(i));
//                    float y = (float) (bounds.centerY() + (bounds.centerX() - 20) * Math.sin(i));
//                    if (i == start) {
//                        path.moveTo(x, y);
//                    } else {
//                        path.lineTo(x, y);
//                    }
//                }
//                canvas.drawPath(path, mLinePaint);